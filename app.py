from flask_script import Manager
from info import create_app, db
from flask_migrate import Migrate, MigrateCommand

app = create_app("dev")
manager = Manager(app)
Migrate(app, db)
manager.add_command("mysql", MigrateCommand)


@app.route("/")
def index():
    return "....."


if __name__ == '__main__':
    manager.run()
