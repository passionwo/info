class Config(object):
    DEBUG = True

    LOGING_DEBUG = DEBUG

    SECRET_KEY = "SECRET_KEY"

    SQLALCHEMY_DATABASE_URI = "mysql://root:mysql@localhost:3306/sql_day"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    REDIS_HOST = "localhost"
    REDIS_PORT = 6379


class DevConfig(Config):
    pass

configs = {
    "dev": DevConfig
}
